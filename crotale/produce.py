from flask import request
from kafka import KafkaConsumer, KafkaProducer
import json


def produce(kafka_url, topic):  # argument: dns of kafka server, topic kafka
    #kafka_url (dans un ficher de conf)
    # create producer pointing kafka server
    producer = KafkaProducer(kafka_url)

    req = request.get_json()
    json_payload = json.dumps(req)
    json_payload = str.encode(json_payload)
    # push data into INFERENCE TOPIC
    producer.send(topic, json_payload)
    return json_payload
