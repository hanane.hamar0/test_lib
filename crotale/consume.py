from kafka import KafkaConsumer
import json

def consume(adresse, topic):
    consumer = KafkaConsumer(adresse)
    consumer.subscribe(topics=topic)
    for message in consumer:
        print("%d:%d: v=%s" % (message.partition,
                               message.offset,
                               message.value))
    return None